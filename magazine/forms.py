import re
from django.core.exceptions import ValidationError
from django import forms
from .models import *

class SupplierForm(forms.ModelForm):
    class Meta:
        model = Supplier
        fields = (
            'name',
            'agent_firstname',
            'agent_name',
            'agent_patronymic',
            'agent_telephone',
            'address',
        )
    def clean_agent_telephone(self):
        agent_telephone = self.cleaned_data['agent_telephone']
        if re.match(r'\+7\(\d{3}\)\d{3}-\d{2}-\d{2}', agent_telephone):
            return agent_telephone
        raise ValidationError('Телефон не соответствует шаблону')


class SupplyForm(forms.ModelForm):
    class Meta:
        model = Supply
        fields = (
            'data_supply',
            'supplier',
        )

class Pos_supplyForm(forms.ModelForm):
    class Meta:
        model = Pos_supply
        fields = (
            'product',
            'supply',
            'count',
        )
class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = (
            'name',
            'description',
        )
class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = (
            'name',
            'description',
        )
class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = (
            'FIO_customer',
            'delivery_address',
            'delivery_type',
            'date_finish'
        )