from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
from django.contrib import messages

# Create your views here.
def home(request):
    return render(request, 'magazine/index.html')

def supplier_list(request):
    list_supplier = Supplier.objects.filter(is_exists=True)
    context = {
        'list_sup': list_supplier
    }
    return render(request, 'magazine/supplier/catalog.html', context)

def supplier_details(request, id):
    supplier = get_object_or_404(Supplier, pk = id)

    context = {
        'supplier_object': supplier
    }
    return render(request, 'magazine/supplier/details.html', context)
def supplier_create(request):
    if request.method == "POST":
        form_supplier = SupplierForm(request.POST)
        if form_supplier.is_valid():
            new_supplier = Supplier(**form_supplier.cleaned_data)
            new_supplier.save()
            messages.success(request, 'Поставщик успешно добавлен')
            return redirect('catalog_supplier_page')
        messages.error(request, 'Неверно заполнены поля')
    else:
        form_supplier = SupplierForm()
    context = {
        'form': form_supplier
    }
    return render(request, 'magazine/supplier/create.html', context)

def product_list(request):
    list_product = Product.objects.filter(exists=True)
    context = {
        'list_product': list_product
    }
    return render(request, 'magazine/product/catalog.html', context)