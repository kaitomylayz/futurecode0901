from django.urls import path, include
from .views import *

urlpatterns = [
    path('index/',home, name='home_page' ),
    path('suppliers/', supplier_list, name='catalog_supplier_page'),
    path('suppliers/details/<int:id>/', supplier_details, name='details_supplier_page'),
    path('suppliers/create/', supplier_create, name='create_supplier_page'),
    path('product/', product_list, name='catalog_product_page'),


]